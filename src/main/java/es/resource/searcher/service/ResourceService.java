package es.resource.searcher.service;

import java.util.List;

import es.resource.searcher.entities.Resource;

public interface ResourceService {

	void insertResource(Resource resource);

	void updateResource(Resource resource);

	void deleteResource(String resource);

	List<Resource> getResources();
	
	Resource getResource(String id);

	List<Resource> findResources(Resource resource);

	long getCount(String id, String source);

	

}
