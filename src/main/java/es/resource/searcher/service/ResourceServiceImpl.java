package es.resource.searcher.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.resource.searcher.constants.ResourceConstants;
import es.resource.searcher.entities.Resource;
import es.valhalla.business.component.AbstractBusinessComponent;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.data.access.mongodb.entities.BaseEntity;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;


@ApplicationScoped
public class ResourceServiceImpl extends AbstractBusinessComponent implements ResourceService {
	
	
	private String url;
	private String databaseName;
	private String user;
	private String secret;
	private String collection;
	
	@PostConstruct
	private void init() {
		final String appName = ResourceConstants.APP_NAME;
		this.url = getStringProperty(appName, ValahallaConstants.MONGO_URL);
		this.databaseName = getStringProperty(ResourceConstants.APP_NAME,
				ValahallaConstants.MONGO_DBNAME);
		this.user = getStringProperty(ResourceConstants.APP_NAME,
				ValahallaConstants.MONGO_USER);
		this.secret = getStringProperty(ResourceConstants.APP_NAME,
				ValahallaConstants.MONGO_SECRET);
		this.collection=getStringProperty(ResourceConstants.APP_NAME,
				ResourceConstants.COLLECTION_NAME);
	}


	@Override
	public void insertResource(final Resource resource) {
		insertMongo(url, user, secret, databaseName, collection, resource, Resource.class);
		

	}

	@Override
	public void updateResource(final Resource resource) {
		updateMongo(url, user, secret, databaseName, collection, resource, Resource.class);
	}

	@Override
	public void deleteResource(String docId) {
		deleteMongo(url, user, secret, databaseName, collection, docId, Resource.class);
	}

	@Override
	public List<Resource> getResources() {
		return getAllMongo(url, user, secret, databaseName, collection,Resource.class);

	}

	@Override
	public Resource getResource(String id) {
		return getMongo(url, user, secret, databaseName, collection, id, Resource.class);
	}

	@Override
	public List<Resource> findResources(final Resource resource) {
		
		final Map<String, Object> mapForMongo = new HashMap<>();

		Map<String, Object> techsToFetch = fetchFromArray(resource.getTechnologies());
		if (techsToFetch != null)
			mapForMongo.put("technologies._id", techsToFetch);

		Map<String, Object> langsToFetch = fetchFromArray(resource.getLanguages());
		if (langsToFetch != null)
			mapForMongo.put("languages._id", langsToFetch);
		return find(url, user, secret, databaseName, collection, Resource.class, mapForMongo);

	}
	
	@Override
	public long getCount(final String id, final String source) {
		final Map<String, Object> mapForMongo = new HashMap<>();
		final Map<String, Object> params = new HashMap<String, Object>();
		final List<String> listItem = new ArrayList<>();
		listItem.add(id);
		params.put(ResourceConstants.MONGO_ALL, listItem);
		mapForMongo.put(source, params);
		return count(url,user,secret,databaseName,collection,mapForMongo);

	}

	
	private <T extends BaseEntity> Map<String, Object> fetchFromArray(List<T> items) {
		Map<String, Object> contains = null;

		if (items != null && !items.isEmpty()) {
			contains = new HashMap<>();
			List<String> fields = new ArrayList<>();
			for (T item : items) {
				fields.add(item.getId());
			}
			contains.put(ResourceConstants.MONGO_ALL, fields);
		}
		return contains;

	}


	

}
