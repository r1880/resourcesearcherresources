package es.resource.searcher.launcher;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import es.resource.searcher.constants.ResourceConstants;
import es.resource.searcher.routes.ResourceEndpoint;
import es.valhalla.business.component.ValhallaServiceRegistryService;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.data.access.mongodb.service.MongoService;
import es.valhalla.entities.ValhallaServiceRegistry;
import es.valhalla.utils.configuration.application.service.ConfigurationApplicationService;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.Http2Settings;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import jakarta.enterprise.inject.spi.CDI;

@ApplicationScoped
public class VertxIntializer {

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(VertxIntializer.class);
	private Vertx vertx;
	private ConfigurationApplicationService configurationApplicationService;
	private String serviceRegistryPath;
	private Map<String, List<ValhallaServiceRegistry>> serviceRegistry = new HashMap<>();

	
	public void itnitalizeVertx() {
		serviceRegistryPath = System.getProperty(ResourceConstants.SERVICE_REGISTRY_PROP);
		serviceRegistryPath = serviceRegistryPath != null ? serviceRegistryPath : ResourceConstants.DEFAULT_REGISTRY_PATH;
		vertx = Vertx.vertx(new VertxOptions().setInternalBlockingPoolSize(200).setEventLoopPoolSize(200));
		readProperties();
		openMongoConnection();
		intializeHttpServer();

	}

	private void intializeHttpServer() {

		LOGGER.info("Deploying http server");
		HttpServer server = vertx.createHttpServer(new HttpServerOptions().setCompressionSupported(true));
		Router router = Router.router(vertx);
		CDI.current().select(ResourceEndpoint.class).get().enableEndpoints(router,serviceRegistry);
		server.requestHandler(router).listen(ResourceConstants.SERVICE_PORT);
		LOGGER.info("Listening on: {}", ResourceConstants.SERVICE_PORT);
		ValhallaServiceRegistryService.selfRegister(vertx, serviceRegistry, serviceRegistryPath,
				ResourceConstants.APP_NAME);


	}

	private void readProperties() {
		LOGGER.info("Initializing properties");
		configurationApplicationService = CDI.current().select(ConfigurationApplicationService.class).get();
		configurationApplicationService.readProperties(ResourceConstants.APP_NAME);
	}

	private void openMongoConnection() {
		LOGGER.info("Initializing mongo connection");
		final MongoService mongoService = CDI.current().select(MongoService.class).get();
		mongoService.intializeConnection(
				configurationApplicationService.getStringProperty(ResourceConstants.APP_NAME,
						ValahallaConstants.MONGO_URL),
				configurationApplicationService.getStringProperty(ResourceConstants.APP_NAME,
						ValahallaConstants.MONGO_USER),
				configurationApplicationService.getStringProperty(ResourceConstants.APP_NAME,
						ValahallaConstants.MONGO_SECRET),
				configurationApplicationService.getStringProperty(ResourceConstants.APP_NAME,
						ValahallaConstants.MONGO_DBNAME));
	}

}
