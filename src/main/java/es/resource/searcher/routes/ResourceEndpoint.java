package es.resource.searcher.routes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import es.resource.searcher.constants.ResourceConstants;
import es.resource.searcher.entities.CountModel;
import es.resource.searcher.entities.Resource;
import es.resource.searcher.service.ResourceService;
import es.valhalla.business.component.AbstractBaseHttpHandler;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.entities.ValhallaServiceRegistry;
import es.valhalla.exception.ValhallaBusinessExceptionEnum;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class ResourceEndpoint extends AbstractBaseHttpHandler {

	@Inject
	ResourceService resourceService;
	
	private Map<String, List<ValhallaServiceRegistry>> serviceRegistry;


	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(ResourceEndpoint.class);

	private enum ENDPOINTS {
		RESOURCES, RESOURCE, DELETE, INSERT, UPDATE, FIND, COUNT
	}

	private void getAll(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.GET.name(), ResourceConstants.SERVICE_PORT,
				ResourceConstants.SERVICE_PATH,getSelfAddress());
		serviceRegistry.get(ResourceConstants.APP_NAME).add(self);
		buildRouteEmpty(router, ResourceConstants.SERVICE_PATH, HttpMethod.GET, MediaType.APPLICATION_JSON)
				.handler(ctx -> proccess(ctx, ENDPOINTS.RESOURCES))
				.failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));

	}

	private void getResource(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.GET.name(), ResourceConstants.SERVICE_PORT,
				ResourceConstants.SERVICE_PATH+ "/resource/:id",getSelfAddress());
		serviceRegistry.get(ResourceConstants.APP_NAME).add(self);
		buildRoute(router, ResourceConstants.SERVICE_PATH + "/resource/:id", HttpMethod.GET, MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON)
		.handler(ctx->proccess(ctx,ENDPOINTS.RESOURCE))
		.failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));
	

	}

	private void deleteResource(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.DELETE.name(), ResourceConstants.SERVICE_PORT,
				ResourceConstants.SERVICE_PATH+ "/resource/:id",getSelfAddress());
		serviceRegistry.get(ResourceConstants.APP_NAME).add(self);
		buildRoute(router, ResourceConstants.SERVICE_PATH+ "/resource/:id" , HttpMethod.DELETE, MediaType.APPLICATION_JSON)
		.handler(ctx->proccess(ctx,ENDPOINTS.DELETE))
		.failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));
		
	}

	private void updateResource(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.PUT.name(), ResourceConstants.SERVICE_PORT,
				ResourceConstants.SERVICE_PATH,getSelfAddress());
		serviceRegistry.get(ResourceConstants.APP_NAME).add(self);	
		buildRoute(router, ResourceConstants.SERVICE_PATH , HttpMethod.PUT, MediaType.APPLICATION_JSON)
		.handler(ctx->proccess(ctx,ENDPOINTS.UPDATE))
		.failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));
	}

	private void insertResource(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.POST.name(), ResourceConstants.SERVICE_PORT,
				ResourceConstants.SERVICE_PATH,getSelfAddress());
		serviceRegistry.get(ResourceConstants.APP_NAME).add(self);
		buildRoute(router, ResourceConstants.SERVICE_PATH , HttpMethod.POST, MediaType.APPLICATION_JSON)
		.handler(ctx->proccess(ctx,ENDPOINTS.INSERT))
		.failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));

	}

	private void findResources(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.POST.name(), ResourceConstants.SERVICE_PORT,
				ResourceConstants.SERVICE_PATH+ "/find",getSelfAddress());
		serviceRegistry.get(ResourceConstants.APP_NAME).add(self);
		buildRoute(router, ResourceConstants.SERVICE_PATH+ "/find" , HttpMethod.POST, MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON)
		.handler(ctx->proccess(ctx,ENDPOINTS.FIND))
		.failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));
	}

	private void count(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.POST.name(), ResourceConstants.SERVICE_PORT,
				ResourceConstants.SERVICE_PATH+ "/count",getSelfAddress());
		serviceRegistry.get(ResourceConstants.APP_NAME).add(self);	
		buildRoute(router, ResourceConstants.SERVICE_PATH+ "/count" , HttpMethod.POST, MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON)
		.handler(ctx->proccess(ctx,ENDPOINTS.COUNT))
		.failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));

	}

	private void proccess(final RoutingContext ctx, final ENDPOINTS endpoint) {
		final long init = System.currentTimeMillis();
		final String token = getTokenFromHeader(ctx.request());
		if (token != null) {	
			final ValhallaServiceRegistry idpService=  serviceRegistry.get(ValahallaConstants.AUTH_SERVICE).stream().filter(s -> s.getServicePath().contains("validate")).findFirst().orElse(null);
			if(idpService==null) {
				LOGGER.error("IDP not registered");
				throw ValhallaBusinessExceptionEnum.IDP_NOT_REGISTERED.getException();
			}
			idpValidation(idpService.getServiceAddress(), idpService.getServicePath(), idpService.getPort(), token).onSuccess(handler -> {
						if (handler.statusCode() == HttpResponseStatus.OK.code()) {
							final String serialized = executedOption(ctx, endpoint);
							processSuccessHttpResponse(ctx, serialized);
							
						} else {
							processUnauthorizedResponse(ctx);
						}

					}).onFailure(handler -> {
						LOGGER.error("Error connecting IDP {}", handler);
						processUnauthorizedResponse(ctx);
					}).onComplete(
							finished -> LOGGER.info("Request processed in {} ms", System.currentTimeMillis() - init));
		} else {
			processUnauthorizedResponse(ctx);
		}
	}

	private String executedOption(final RoutingContext ctx, final ENDPOINTS endpoint) {
		String serialized = null;
		switch (endpoint) {

		case RESOURCES:

			serialized = serialize(resourceService.getResources());
			break;
		case RESOURCE:
			serialized = serialize(resourceService.getResource(ctx.pathParam(ResourceConstants.ID)));
			break;
		case INSERT:
			resourceService.insertResource(deserialize(ctx.getBodyAsString(), Resource.class));

			break;
		case UPDATE:
			resourceService.updateResource(deserialize(ctx.getBodyAsString(), Resource.class));

			break;
		case DELETE:
			resourceService.deleteResource(ctx.pathParam(ResourceConstants.ID));

			break;
		case FIND:
			serialized = serialize(resourceService.findResources(deserialize(ctx.getBodyAsString(), Resource.class)));

			break;
		case COUNT:
			final CountModel cm = deserialize(ctx.getBodyAsString(), CountModel.class);
			serialized = serialize(resourceService.getCount(cm.getId(), cm.getSource()));

			break;
		}
		return serialized;
	}

	public void enableEndpoints(final Router router, final Map<String, List<ValhallaServiceRegistry>> serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
		serviceRegistry.put(ResourceConstants.APP_NAME, new ArrayList<>());
		getAll(router);
		getResource(router);
		deleteResource(router);
		updateResource(router);
		insertResource(router);
		findResources(router);
		count(router);
	}

}
