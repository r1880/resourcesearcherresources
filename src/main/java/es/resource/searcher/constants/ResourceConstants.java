package es.resource.searcher.constants;

public class ResourceConstants {

	public static final String APP_NAME = "Resources";

	public static final String DATABASE_NAME = "mongo.database.name";
	
	public static final String COLLECTION_NAME = "mongo.collection";

	public static final String MONGO_ALL = "$all";

	public static final String SERVICE_PATH = "/resourcesearcher/rest/service/resources";

	public static final String ID = "id";

	public static final int SERVICE_PORT = 8081;

	public static final String SERVICE_REGISTRY_PROP = "service.registry.path";
	/** LOCAL PROPERTY **/
	public static final String DEFAULT_REGISTRY_PATH = "C:\\ResoureSearcher\\resource_searcher.json";

}
